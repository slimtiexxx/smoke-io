/**
 * Created by andras on 21/02/17.
 */

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

users = [];
connections = [];

server.listen(process.env.PORT || 3000);

console.log('Server running ');



//  res.sendFile(__dirname + '/index.html');
app.get('/', function (req, res) {
   res.sendFile(__dirname + '/sitebuild/index.html');
});

app.use(express.static('sitebuild'))

io.sockets.on('connection', function (socket) {
    connections.push(socket);
    console.log('Connected: %s sockets connected', connections.length);
    updateUsernames();

    // Disconnect
    socket.on('disconnect', function (data) {

        users.splice(users.indexOf(socket.username), 1);
        updateUsernames();

        connections.splice(connections.indexOf(socket), 1);
        console.log('Disconnected: %s sockets left connected', connections.length);
    });


    // Send Data
    socket.on('send message', function (data) {
        io.sockets.emit('new message', {msg: data, user: socket.username});
    });



    socket.on('sending', function (data) {
        io.socket.emit('getid', {prop: data, user: socket.username})
    });


    // New User
    socket.on('new user', function (data, callback) {
        callback(true);
        socket.username = data;
        users.push({username: socket.username, status: 'waiting'});
        updateUsernames();
    });

    // Update Usernames
    function updateUsernames() {
        io.sockets.emit('get users', users)
    }

    socket.on('valuechange', function (data) {

        var user = users.find(function(item){
            return item.username == socket.username;
        });

        console.log(data);

        user.status = data;


        io.emit('XXX', users);
    });

    socket.on('moveLift', function (data) {
        console.log(data);
        io.emit('liftmove', data)
    })
});